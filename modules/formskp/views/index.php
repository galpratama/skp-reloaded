<?php $user = $this->ion_auth->get_data_user_by_id(); ?>
   <ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="<?php echo base_url(); ?>">Home</a> 
        <i class="icon-angle-right"></i>
    </li>     
    <li><a href="#">Target/Sasaran Kerja Pegawai</a></li>
</ul>
<h5 class="text-center title">Daftar Pegawai Pemerintah Kabupaten Bandung</h5> 
<div class="row ">
<form action="<?php echo base_url(); ?>skp/cetak/multiple_id_convert" method="post">
    <div class="pull-right">
        <select name="tahun"  id="tahun" >
                    <option value="<?php echo date('Y'); ?>" ><?php echo date('Y'); ?></option>
                    <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                    <option value="<?php echo date('Y') + 2; ?>"><?php echo date('Y') + 2; ?></option>
                </select>
        <button type="submit" class="btn btn-sm btn-primary"><i class="icon-print"></i> Cetak Data Yang Dicentang</button>
    </div>
        <input type="hidden" id="group_id" value="<?php echo $user->group_id ?>">
        <table id='stattable' class="table table-bordered table-striped table-condensed flip-content ">    
        <thead  >
            <tr>   
                <th><input type="text"  style="display: none"  /></th>
<!--                <th id="nil"><input type="text" style="display: none"   /></th>-->
                <th><input type="text" style="display: none"   /></th>              
                <th><input type="text" id="namafilter" class="noborder" onchange="tampil()"  /></th>
                <th><input type="text"  id="nipfilter" class="noborder" onchange="tampil()"  /></th>
                <th width="10%"><input type="text" onchange="tampil()" id="jabatanfilter" class="noborder" /></th>
                <th><select  id="golonganfilter" class="noborder"  multiple="true" onchange="tampil()" >
                        <?php foreach ($listgol as $gol) {
                            ?>
                            <option value="<?php echo $gol->gol_id; ?>"><?php echo $gol->nm_gol; ?></option>
                        <?php } ?>
                    </select></th>
                <th><select  id="eselonfilter" class="noborder" multiple="true" onchange="tampil()" >
                        <?php foreach ($listeselon as $es) {
                            ?>
                            <option value="<?php echo $es->eselon_id; ?>"><?php echo $es->eselon_nm; ?></option>
                        <?php } ?>
                    </select></th>
                <th><input type="text"  id="instansifilter" class="noborder" /></th>
            </tr>
            <tr>   
                <th>Entri</th>
<!--                <th id="nil">Nilai</th>-->
                <th>Cetak</th>              
                <th>Nama</th>
                <th>NIP</th>
                <th width="10%">Jabatan</th>
                <th>Golongan</th>
                <th>Eselon</th>
                <th>Instansi</th>
            </tr>
        </thead>
        <tbody>             
        </tbody> 
    </table>    
    </form>
</div>

<script src="<?php echo base_url(); ?>static/js/aplikasi/formskp/skpjs.js" type="text/javascript"></script> 
