<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of pembuatan
 *
 * @author hari
 */
class cetak extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->ion_auth->check_uri_permissions();
        $this->load->model('skp_model', 'skp_model');
        $this->load->model('formskp/Skpmodel', 'skpmodel');
    }

    public function index($peg_id) {

        $status = 1;
        $tahun = date('Y');
        $data['skp_pokok'] = $this->skp_model->get_skp_all($peg_id, $tahun, $status);
        if($data['skp_pokok']){
            $data['pegawai'] = $this->skpmodel->getdatapegawaidetail($peg_id);
            $this->template->load('mainlayout', 'cetak', $data);
        }else{
            echo "<script>alert('Belum ada entri.')</script>";
            header("Refresh: 1;url=".base_url()."formskp/formskpcontroller/index");
        }
    }

    public function changeState($peg_id,$thn) {
        $this->skpmodel->updatestate($peg_id,$thn);
    }

    public function cetak_multiple($peg_id,$tahun){

        $data['q_pegawai'] = $this->skp_model->get_pegawai($peg_id);
        $data['pegawai'] = $data['q_pegawai']->row();            

        $status = 1;
        $data['skp_pokok'] = $this->skp_model->get_skp_all($peg_id, $tahun, $status);

        $status = 2;
        $data['skp_tambahan'] = $this->skp_model->get_skp_all($peg_id, $tahun, $status);
            
        // ambil data pegawai yang menilai
        foreach ($data['skp_pokok'] as $dp) {
            $peg_id_atasan = $dp->peg_id_atasan;
        }

        if($data['skp_pokok']){

        }else{
            echo "<script>alert('Belum ada entri.')</script>";
            header("Refresh: 1;url=".base_url()."formskp/formskpcontroller/index");
            break;
        }

        $data['q_pegawai_atasan'] = $this->skp_model->get_pegawai($peg_id_atasan);
        $data['pegawai_atasan'] = $data['q_pegawai_atasan']->row();
            
        $this->template->load('mainlayout', 'cetak_multiple', $data);        
    }

    public function multiple_id_convert()
    {
        if ($this->input->post('multiple')){
           $url = implode('-',$this->input->post('multiple'));
           // var_dump($this->input->post('multiple'));
           redirect('skp/cetak/multiple_print/' . $url . '/' . $this->input->post('tahun'));
              
        } else {
            redirect('formskp/formskpcontroller/index');
        }
    }

    public function multiple_id_convert_ppk()
    {
        if ($this->input->post('multiple')){
           $url = implode('-',$this->input->post('multiple'));
           // var_dump($this->input->post('multiple'));
           redirect('skp/cetak/multiple_print/' . $url . '/' . $this->input->post('tahun'));
              
        } else {
            redirect('ppk/penilaian/index');
        }
    }

    public function multiple_print(){

        // pecah peg_id dari url
        $peg_id = explode('-', $this->uri->segment(4));
        // ambil tahun dari url
        $tahun = $this->uri->segment(5);

        // hitung banyak array di dalam variable $peg_id
        $peg_id_count = count($peg_id) - 1;
        $data['i'] = $peg_id_count;
        // looping data dan dimasukkan ke dalam $data['skp_pokok'][$i]
        $i = 0;
        foreach ($peg_id as $data_peg) {

            // echo "<h1>". $i ."</h1>";

            // ambil data pegawai yang dinilai
            $data['q_pegawai'][$i] = $this->skp_model->get_pegawai($peg_id[$i]);
            $data['pegawai'][$i] = $data['q_pegawai'][$i]->row();
                
                // dump
                // echo "<h3>Pegawai Yang Dinilai </h3>";
                // var_dump($data['pegawai'][$i]);

            // ambil skp all
            $data['skp_pokok'][$i] = $this->skp_model->get_skp_all($peg_id[$i], $tahun);
            $status = 2;
            $data['skp_tambahan'][$i] = $this->skp_model->get_skp_all($peg_id[$i], $tahun, $status);
                
                // dump
                // echo "<h3>Data SKP Pokok & Tambahan <br><small>(Status: pokok = 1 , tambahan = 2)</small></h3>";
                // var_dump($data['skp_pokok'][$i]);

            // ambil data pegawai yang menilai
            foreach ($data['skp_pokok'][$i] as $dp) {
                $peg_id_atasan = $dp->peg_id_atasan;
            }

            if($data['skp_pokok'][$i]){

            }else{
                echo "<script>alert('Belum ada entri.')</script>";
                header("Refresh: 1;url=".base_url()."formskp/formskpcontroller/index");
                break;
            }

            $data['q_pegawai_atasan'][$i] = $this->skp_model->get_pegawai($peg_id_atasan);
            $data['pegawai_atasan'][$i] = $data['q_pegawai_atasan'][$i]->row();
                
                // dump
            //     echo "<h3>Pegawai Atasan Yang Menilai</h3>";
            //     var_dump($data['pegawai_atasan'][$i]);

            // echo "<hr>";
            // $this->template->load('mainlayout', 'cetak_multiple', $data);
                echo "<script>
                    window.open('".base_url()."index.php/skp/cetak/cetak_multiple/".$peg_id[$i]."/".$tahun."','_blank','height='+screen.height+', width='+screen.width);
                </script>";
            // increment
            $i++; 
        }

        header("Refresh: 1;url=".base_url()."formskp/formskpcontroller/index");
    }

}
