<hr class="hidden-print">
<ul class="page-breadcrumb breadcrumb hidden-print">
    <li>
        <i class="icon-home"></i>
        <a href="<?php echo base_url(); ?>">Home</a>
        <i class="icon-angle-right"></i>
    </li>
    <li>
        Cetak SKP
    </li>
</ul>
<div class="row">
    <form role="form" enctype='multipart/form-data'  action="<?php echo base_url(); ?>skp/pembuatan/tambahkegiatan" method="post">
        <div class="col-sm-12">
            <h3 class="hidden-print">
                <span class="hidden-print">Cetak Sasaran Kinerja Pegawai (SKP)</span>
                <a class="btn btn-md btn-primary pull-right hidden-print" onClick="cetak()"><i class="glyphicon glyphicon-print"></i> Cetak SKP</a>
            </h3>
             <div class="row col-sm-12 hidden-print">
                Tahun : 
                <select name="tahun"  id="tahun" >
                    <option value="<?php echo date('Y'); ?>" ><?php echo date('Y'); ?></option>
                    <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                    <option value="<?php echo date('Y') + 2; ?>"><?php echo date('Y') + 2; ?></option>
                </select></div><br/>
            <br class="hidden-print">
            <div class="row col-sm-12 text-center title-print">
                <h3>PENILAIAN SASARAN KINERJA PEGAWAI (SKP)</h3>
                <h4>PEGAWAI NEGERI SIPIL</h4>
            </div>
            <div class='col-sm-6'>
                <table class="col-sm-12 table table-bordered" id="tblPenilai">
                    <tr>
                        <th width="10%">No</th>
                        <th colspan="2">I. Pejabat Penilai</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td width="20%">Nama</td>
                        <td>
                            <input type="hidden" id="hiddenidpjabatpenilai"  name="hiddenidpjabatpenilai" />
                            <span id="txtnamapejabatpenilai"><?php echo $pegawai_atasan->peg_nama ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>NIP</td>
                        <td> <span id="txtnippejabatpenilai"><?php echo $pegawai_atasan->peg_nip_baru ?></span> </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Jabatan</td>
                        <td><span id="txtjabatan"><?php echo $pegawai_atasan->jabatan_nama ?></span></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Pangkat</td>
                        <td><span id="txtpangkat"><?php echo $pegawai_atasan->nm_gol_akhir ?>/<?php echo $pegawai_atasan->nm_pkt_akhir; ?></span></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Unit Kerja</td>
                        <td><span id="txtunitkerja"><?php echo $pegawai_atasan->cabang_dinas; ?> - <?php echo $pegawai_atasan->satuan_kerja_nama; ?></span></td>
                    </tr>
                </table>
            </div>
            <div class='col-sm-6'>


                <table class="col-sm-12 table table-bordered" id="tblDinilai">
                    <tr>
                        <th width="10%">No</th>
                        <th colspan="2">II. Pegawai Negeri Sipil Yang Dinilai</th>

                    </tr>
                    <tr>
                        <td>1</td>
                        <td width="20%">Nama</td>
                        <td id="dinilaiName"><?php echo $pegawai->peg_nama; ?></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>NIK</td>
                        <td id="dinilaiNik"><?php echo $pegawai->peg_nip_baru; ?><input type="hidden" name="nikPegawai"  id="nikPegawai"  value="<?php echo $pegawai->peg_id; ?>"></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Jabatan</td>
                        <td id="dinilaiJab"><?php echo $pegawai->jabatan_nama; ?></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Pangkat</td>
                        <td id="dinilaiPan"><?php echo $pegawai->nm_gol_akhir; ?>/<?php echo $pegawai->nm_pkt_akhir; ?></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Unit Kerja</td>
                        <td id="dinilaiUK"><?php echo $pegawai->cabang_dinas; ?> - <?php echo $pegawai->satuan_kerja_nama; ?></td>
                    </tr>
                </table>

            </div>
        </div>
    </form>
</div>
<div class='row'>
    <div class='col-sm-12'>
        <h3>
            Kegiatan Tugas Pokok Jabatan
        </h3>
        <hr>

        <table class="table table-bordered table-stripped" id="tblJabatan">
            <thead>
                <tr>
                    <th width="1%" rowspan="2">No.</th>
                    <th rowspan="2" width="30%">Kegiatan Tugas Pokok Jabatan</th>
                    <th rowspan="2">Angka Kredit</th>
                    <th colspan="4">Target</th>
                    <th colspan="4">Realisasi</th>
                </tr>
                <tr>
                    <th>Kuantitas <br><i>Output/Satuan</i></th>
                    <th>Kual / Mutu</th>
                    <th>Waktu <br><i>Satuan Waktu</i></th>
                    <th>Biaya</th>
                    <th>Kuantitas <br><i>Output/Satuan</i></th>
                    <th>Kual / Mutu</th>
                    <th>Waktu <br><i>Satuan Waktu</i></th>
                    <th>Biaya</th>
                </tr>
            </thead>
            <tbody id="bodykpi">
                <?php 
                    $i = 1;
                    foreach($skp_pokok as $skp_row){ 
                     ?>
                    <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $skp_row->deskripsi_kegiatan; ?></td>
                        <td><?php echo $skp_row->nilai_angka_kredit ?></td>
                        <td><?php echo $skp_row->target_kuantitatif ?> <?php echo $skp_row->satuan_target_kuantitatif ?></td>
                        <td><?php echo $skp_row->target_kualitas ?> <?php echo $skp_row->satuan_target_kualitas ?></td>
                        <td><?php echo $skp_row->waktu ?> <?php echo $skp_row->satuan_waktu ?></td>
                        <td><?php echo $skp_row->biaya ?> <?php echo $skp_row->satuan_biaya ?></td>
                        <td><?php echo $skp_row->realisasi_kuantitatif ?> <?php echo $skp_row->satuan_target_kuantitatif ?></td>
                        <td><?php echo $skp_row->realisasi_kualitas ?> <?php echo $skp_row->satuan_target_kualitas ?></td>
                        <td><?php echo $skp_row->realisasi_waktu ?> <?php echo $skp_row->satuan_waktu ?></td>
                        <td><?php echo $skp_row->realisasi_biaya ?> <?php echo $skp_row->satuan_biaya ?></td>
                    </tr>
                <?php $i++; } ?>
            </tbody>            
        </table>

    </div>
    <div class="col-sm-12">
        <h3>Kegiatan Tugas Pokok Tambahan</h3>
        <hr>       
        <table class="table table-bordered table-stripped" id="tblTambahan">
            <thead>
                <tr>
                    <th width="20px" rowspan="2">No.</th>
                    <th rowspan="2" width="30%">Kegiatan Tugas Pokok Tambahan</th>
                    <th rowspan="2">Angka Kredit</th>
                    <th colspan="4">Target</th>
                    <th colspan="4">Realisasi</th>
                </tr>
                <tr>
                    <th>Kuantitas <br><i>Output/Satuan</i></th>
                    <th>Kual / Mutu</th>
                    <th>Waktu <br><i>Satuan Waktu</i></th>
                    <th>Biaya</th>
                    <th>Kuantitas <br><i>Output/Satuan</i></th>
                    <th>Kual / Mutu</th>
                    <th>Waktu <br><i>Satuan Waktu</i></th>
                    <th>Biaya</th>
                </tr>
            </thead>
            <tbody id="bodykpitambahan">   
                <?php 
                    $i = 1;
                     foreach($skp_tambahan as $skp_row){ ?>
                    <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $skp_row->deskripsi_kegiatan; ?></td>
                        <td><?php echo $skp_row->nilai_angka_kredit ?></td>
                        <td><?php echo $skp_row->target_kuantitatif ?> <?php echo $skp_row->satuan_target_kuantitatif ?></td>
                        <td><?php echo $skp_row->target_kualitas ?> <?php echo $skp_row->satuan_target_kualitas ?></td>
                        <td><?php echo $skp_row->waktu ?> <?php echo $skp_row->satuan_waktu ?></td>
                        <td><?php echo $skp_row->biaya ?> <?php echo $skp_row->satuan_biaya ?></td>
                        <td><?php echo $skp_row->realisasi_kuantitatif ?> <?php echo $skp_row->satuan_target_kuantitatif ?></td>
                        <td><?php echo $skp_row->realisasi_kualitas ?> <?php echo $skp_row->satuan_target_kualitas ?></td>
                        <td><?php echo $skp_row->realisasi_waktu ?> <?php echo $skp_row->satuan_waktu ?></td>
                        <td><?php echo $skp_row->realisasi_biaya ?> <?php echo $skp_row->satuan_biaya ?></td>
                    </tr>
                <?php $i++; } ?>             
            </tbody>
        </table>
    </div>
    <!-- <div class="col-sm-12">
        <div class="input-group pull-right">
            <button type="submit" class="btn btn-primary btn-lg"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
            <button type="reset" class="btn btn-danger btn-lg"><i class="glyphicon glyphicon-trash"></i> Ulangi</button>
        </div>
    </div>  -->
</div>

<div id="modal_edit_kegiatan" class="modal fade in" style="display: none;">  
    <div class="modal-dialog modal-dialog-center modal-wide">
        <div class="modal-content">
            <div class="modal-header" >   
                <h3><i class="glyphicon glyphicon-briefcase"></i> Form Penilaian SKP</h3>  
            </div>  
            <div class="modal-body"  >  
                <div id="modal-edit">
                    
                </div>
                <form>
                    <input type="hidden" name="id_kegiatan" id="id_kegiatan"  />

                    <div class="col-sm-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Nama Kegiatan</th>
                                    <th>AK</th>
                                    <th>Kuantitas/Output</th>
                                    <th>Kualitas/Mutu</th>
                                    <th>Waktu</th>
                                    <th>Biaya</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="bodytarget">
                                
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-12">
                        <hr>
                        <h4><i class="icon-pencil"></i> Isian Realisasi</h4>
                        <div class="input-group col-sm-12">
                            <label for="deskripsi_kegiatan">Nama Kegiatan</label>
                            <input type="hidden" name="e_id_kegiatan" id="e_id_kegiatan">
                            <input type="text" disabled='disabled' class="form-control" name="e_deskripsi_kegiatan"  id="e_deskripsi_kegiatan" size="45" />
                        </div>
                        <div class="input-group col-sm-12">
                            <label for="target_kuantitatif">Kuantitas/Output</label> 
                            <input type="text" class="form-control" name="e_realisasi_kuantitatif"  id="e_realisasi_kuantitatif" size="25" />
                            <input type="hidden" class="form-control" name="e_satuan_realisasi_kuantitatif" id="e_satuan_realisasi_kuantitatif">
                            <input type="text" disabled='disabled' class="form-control" name="e_satuan_kuantitatif" id="e_satuan_kuantitatif">
                        </div>
                        <div class="input-group col-sm-12">
                            <label for="target_kualitas">Kualitas/Mutu (Dalam persen %)</label> 
                            <input type="text" class="form-control" name="e_realisasi_kualitas"  id="e_realisasi_kualitas"  />
                        </div> 
                        <div class="input-group col-sm-12">
                            <label for="waktu">Waktu</label> 
                            <input type="text" class="form-control" name="e_waktu"  id="e_waktu"   />
                            <input type="hidden" class="form-control" name="e_satuan_realisasi_waktu" id="e_satuan_realisasi_waktu">
                            <input type="text" disabled='disabled' class="form-control" name="e_satuan_waktu" id="e_satuan_waktu">
                        </div>
                        <div class="input-group col-sm-12">
                            <label for="biaya">Biaya (Rp.)</label>
                            <input type="text" class="form-control" name="e_biaya"  id="e_biaya"   />
                        </div>
                        <div class="input-group col-sm-12">
                            <label for="status">Status</label> 
                            <select id="e_status" class="form-control"  name="e_status">
                                <option value="0">-</option>
                                <option value="1">Tugas Pokok</option>
                                <option value="2">Tugas Tambahan</option>
                            </select>
                        </div>
                    </div>                    
                </form>           
            </div>  
            <div class="modal-footer"  >  
                <button class="btn btn-primary btn-large" onclick="simpanrealisasi()">Simpan</button>  
                <a href="#" class="btn btn-primary btn-large" data-dismiss="modal">Close</a>  
            </div>  
        </div>
    </div>  
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>static/js/aplikasi/formskp/cetakskp.js" ></script>
<script>
    $(document).ready(function(){
        window.print();
    });
</script>
<style>
    #action-button {
        visibility: hidden;
        display: none;
        width: 0px;
        height: 0px;
    }
</style>