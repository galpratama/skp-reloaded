<hr class="hidden-print">
<ul class="page-breadcrumb breadcrumb hidden-print">
    <li>
        <i class="icon-home"></i>
        <a href="<?php echo base_url(); ?>">Home</a>
        <i class="icon-angle-right"></i>
    </li>
    <li>
        Cetak SKP
    </li>
</ul>
<div class="row">
    <form role="form" enctype='multipart/form-data'  action="<?php echo base_url(); ?>skp/pembuatan/tambahkegiatan" method="post">
        <div class="col-sm-12">
            <h3 class="hidden-print">
                <span class="hidden-print">Cetak Sasaran Kinerja Pegawai (SKP)</span>
                <a class="btn btn-md btn-primary pull-right hidden-print" onClick="cetak()"><i class="glyphicon glyphicon-print"></i> Cetak SKP</a>
            </h3>
             <div class="row col-sm-12 hidden-print">
                Tahun : 
                <select name="tahun"  id="tahun" onchange="ambildata()" >
                    <option value="<?php echo date('Y'); ?>" ><?php echo date('Y'); ?></option>
                    <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                    <option value="<?php echo date('Y') + 2; ?>"><?php echo date('Y') + 2; ?></option>
                </select></div><br/>
            <br class="hidden-print">
            <div class="row col-sm-12 text-center title-print">
                <h3>PENILAIAN SASARAN KINERJA PEGAWAI (SKP)</h3>
                <h4>PEGAWAI NEGERI SIPIL</h4>
            </div>
            <div class='col-sm-6'>
                <table class="col-sm-12 table table-bordered" id="tblPenilai">
                    <tr>
                        <th width="10%">No</th>
                        <th colspan="2">I. Pejabat Penilai</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td width="20%">Nama</td>
                        <td>
                            <input type="hidden" id="hiddenidpjabatpenilai"  name="hiddenidpjabatpenilai" />
                            <span id="txtnamapejabatpenilai"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>NIP</td>
                        <td> <span id="txtnippejabatpenilai"></span> </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Jabatan</td>
                        <td><span id="txtjabatan"></span></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Pangkat</td>
                        <td><span id="txtpangkat"></span></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Unit Kerja</td>
                        <td><span id="txtunitkerja"></span></td>
                    </tr>
                </table>
            </div>
            <div class='col-sm-6'>


                <table class="col-sm-12 table table-bordered" id="tblDinilai">
                    <tr>
                        <th width="10%">No</th>
                        <th colspan="2">II. Pegawai Negeri Sipil Yang Dinilai</th>

                    </tr>
                    <tr>
                        <td>1</td>
                        <td width="20%">Nama</td>
                        <td id="dinilaiName"><?php echo $pegawai->peg_nama; ?></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>NIK</td>
                        <td id="dinilaiNik"><?php echo $pegawai->peg_nip_baru; ?><input type="hidden" name="nikPegawai"  id="nikPegawai"  value="<?php echo $pegawai->peg_id; ?>"></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Jabatan</td>
                        <td id="dinilaiJab"><?php echo $pegawai->jabatan_nama; ?></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Pangkat</td>
                        <td id="dinilaiPan"><?php echo $pegawai->nm_gol_akhir; ?>/<?php echo $pegawai->nm_pkt_akhir; ?></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Unit Kerja</td>
                        <td id="dinilaiUK"><?php echo $pegawai->cabang_dinas; ?> - <?php echo $pegawai->satuan_kerja_nama; ?></td>
                    </tr>
                </table>

            </div>
        </div>
    </form>
</div>
<div class='row'>
    <div class='col-sm-12'>
        <h3>
            Kegiatan Tugas Pokok Jabatan <input type="hidden" value="" id="isrealised">
        </h3>
        <hr>

        <table class="table table-bordered table-stripped" id="tblJabatan">
            <thead>
                <tr>
                    <th width="1%" rowspan="2">No.</th>
                    <th rowspan="2" width="30%">Kegiatan Tugas Pokok Jabatan</th>
                    <th rowspan="2">Angka Kredit</th>
                    <th colspan="4">Target</th>
                    <th colspan="4" class="rel">Realisasi</th>
                </tr>
                <tr>
                    <th>Kuantitas <br><i>Output/Satuan</i></th>
                    <th>Kual / Mutu</th>
                    <th>Waktu <br><i>Satuan Waktu</i></th>
                    <th>Biaya</th>
                    <th class="rel">Kuantitas <br><i>Output/Satuan</i></th>
                    <th class="rel">Kual / Mutu</th>
                    <th class="rel">Waktu <br><i>Satuan Waktu</i></th>
                    <th class="rel">Biaya</th>
                </tr>
            </thead>
            <tbody id="bodykpi"></tbody>
            <!-- <tfoot>
                <tr>
                    <th colspan="12">
            <div class='input-group pull-right'>
                <button  onclick="tugaspokok(1)" data-toggle="modal" data-target="#modal_pokok_jabatan" type="reset" class="btn btn-success btn-sm "><i class="glyphicon glyphicon-plus"></i> Tambah Realisasi Kegiatan Pokok</button>
            </div>
            </th>
            </tr>
            </tfoot> -->
        </table>

    </div>
    <div class="col-sm-12">
        <h3>Kegiatan Tugas Pokok Tambahan</h3>
        <hr>       
        <table class="table table-bordered table-stripped" id="tblTambahan">
            <thead>
                <tr>
                    <th width="20px" rowspan="2">No.</th>
                    <th rowspan="2" width="30%">Kegiatan Tugas Pokok Tambahan</th>
                    <th rowspan="2">Angka Kredit</th>
                    <th colspan="4">Target</th>
                    <th colspan="4" class="relt">Realisasi</th>
                </tr>
                <tr>
                    <th>Kuantitas <br><i>Output/Satuan</i></th>
                    <th>Kual / Mutu</th>
                    <th>Waktu <br><i>Satuan Waktu</i></th>
                    <th>Biaya</th>
                    <th class="relt">Kuantitas <br><i>Output/Satuan</i></th>
                    <th class="relt">Kual / Mutu</th>
                    <th class="relt">Waktu <br><i>Satuan Waktu</i></th>
                    <th class="relt">Biaya</th>
                </tr>
            </thead>
            <tbody id="bodykpitambahan">                
            </tbody>
            <!-- <tfoot>
                <tr>
                    <th colspan="12">
            <div class='input-group pull-right'>
                <button onclick="tugaspokok(2)" data-toggle="modal" data-target="#modal_pokok_jabatan" type="reset" class="btn btn-success btn-sm "><i class="glyphicon glyphicon-plus"></i> Tambah Realisasi Kegiatan Tugas Tambahan</button>
            </div>
            </th>
            </tr>
            </tfoot> -->
        </table>
    </div>
    <!-- <div class="col-sm-12">
        <div class="input-group pull-right">
            <button type="submit" class="btn btn-primary btn-lg"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
            <button type="reset" class="btn btn-danger btn-lg"><i class="glyphicon glyphicon-trash"></i> Ulangi</button>
        </div>
    </div>  -->
</div>

<div id="modal_edit_kegiatan" class="modal fade in" style="display: none;" >  
    <div class="modal-dialog modal-dialog-center modal-wide">
        <div class="modal-content">
            <div class="modal-header" >   
                <h3><i class="glyphicon glyphicon-briefcase"></i> Form SKP</h3>  
            </div>  
            <div class="modal-body"  >  
                <div id="modal-edit">
                    
                </div>
                <form>
                    <input type="hidden" name="id_kegiatan" id="id_kegiatan"  />

                    <div class="col-sm-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Nama Kegiatan</th>
                                    <th>AK</th>
                                    <th>Kuantitas/Output</th>
                                    <th>Kualitas/Mutu</th>
                                    <th>Waktu</th>
                                    <th>Biaya</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="bodytarget">
                                
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-12">
                        <hr>
                        <h4><i class="icon-pencil"></i> Isian Realisasi</h4>
                        <div class="input-group col-sm-12">
                            <label for="deskripsi_kegiatan">Nama Kegiatan</label>
                            <input type="hidden" name="e_id_kegiatan" id="e_id_kegiatan">
                            <input type="text" disabled='disabled' class="form-control" name="e_deskripsi_kegiatan"  id="e_deskripsi_kegiatan" size="45" />
                        </div>
                        <div class="input-group col-sm-12">
                            <label for="target_kuantitatif">Kuantitas/Output</label> 
                            <input type="text" class="form-control" name="e_realisasi_kuantitatif"  id="e_realisasi_kuantitatif" size="25" />
                            <input type="hidden" class="form-control" name="e_satuan_realisasi_kuantitatif" id="e_satuan_realisasi_kuantitatif">
                            <input type="text" disabled='disabled' class="form-control" name="e_satuan_kuantitatif" id="e_satuan_kuantitatif">
                        </div>
                        <div class="input-group col-sm-12">
                            <label for="target_kualitas">Kualitas/Mutu (Dalam persen %)</label> 
                            <input type="text" class="form-control" name="e_realisasi_kualitas"  id="e_realisasi_kualitas"  />
                        </div> 
                        <div class="input-group col-sm-12">
                            <label for="waktu">Waktu</label> 
                            <input type="text" class="form-control" name="e_waktu"  id="e_waktu"   />
                            <input type="hidden" class="form-control" name="e_satuan_realisasi_waktu" id="e_satuan_realisasi_waktu">
                            <input type="text" disabled='disabled' class="form-control" name="e_satuan_waktu" id="e_satuan_waktu">
                        </div>
                        <div class="input-group col-sm-12">
                            <label for="biaya">Biaya (Rp.)</label>
                            <input type="text" class="form-control" name="e_biaya"  id="e_biaya"   />
                        </div>
                        <div class="input-group col-sm-12">
                            <label for="status">Status</label> 
                            <select id="e_status" class="form-control"  name="e_status">
                                <option value="0">-</option>
                                <option value="1">Tugas Pokok</option>
                                <option value="2">Tugas Tambahan</option>
                            </select>
                        </div>
                    </div>                    
                </form>           
            </div>  
            <div class="modal-footer"  >  
                <button class="btn btn-primary btn-large" onclick="simpanrealisasi()">Simpan</button>  
                <a href="#" class="btn btn-primary btn-large" data-dismiss="modal">Close</a>  
            </div>  
        </div>
    </div>  
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>static/js/aplikasi/formskp/cetakskp.js" ></script>
<style>
    #action-button {
        visibility: hidden;
        display: none;
        width: 0px;
        height: 0px;
    }
    @media print{
        .title-print{
            margin-top:-70px;
        }
    }
</style>

<script>
    function cetak(){
        window.print();
        var isr = $('#isrealised').val();
        if(isr == 'Yes'){
            var peg_id = $('#nikPegawai').val();
            var thn = $('#tahun').val();
            var url = getbasepath() + "skp/cetak/changeState/" + peg_id + "/" + thn;
            $.post(url);
        }
    }
</script>