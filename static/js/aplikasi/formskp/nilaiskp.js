$(document).ready(function () {
    ambildatatugas(1);
    ambildatatugas(2);
    getpjabatpenilai();
});

function ambildata(){
    ambildatatugas(1);
    ambildatatugas(2);
    getpjabatpenilai();
}
// $('#bodykpitambahan').ready(function(){
//     window.print();
// });

function tugaspokok(id) {
    $("#status").val(id);
}

function submitdata() {
    var pegnip = $("#hiddenidpjabatpenilai").val();
    if (pegnip != '') {
        simpandata( );
    } else {
        bootbox.alert("Pejabat penilai wajib diisi !");
    }
}

function updatedata() {
    var pegnip = $("#hiddenidpjabatpenilai").val();
    if (pegnip != '') {
        updatedata( );
    } else {
        bootbox.alert("Pejabat penilai wajib diisi !");
    }
}

function getpjabatpenilai() {
      var varurl = getbasepath() + "skp/pembuatan/getpejabatpenilai/" + $("#nikPegawai").val() + "/" + $("#tahun").val()
    $.post(varurl, {},
            {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }}, "json").done(
            function (hasil) {
                var data = hasil.datahasil;
                $("#hiddenidpjabatpenilai").val(data.peg_id);
                $("#txtnamapejabatpenilai").html(data.nama_lengkap);
                $("#txtnippejabatpenilai").html(data.peg_nip_baru);
                $("#txtjabatan").html(data.jabatan_nama);
                $("#txtpangkat").html(data.nm_gol_akhir + "/" + data.nm_pkt_akhir);
                var unit_cabang = data.unit_kerja_nama;
                if (data.cabang_dinas != '' && data.cabang_dinas != 'null' && data.cabang_dinas != null)
                    unit_cabang = data.cabang_dinas;
                $("#txtunitkerja").html(unit_cabang + " - " + data.satuan_kerja_nama);
            });
}
function ambildatatugas(status) {
    var varurl = getbasepath() + "skp/penilaian/gettugas/" + $("#nikPegawai").val() + "/" + $("#tahun").val() + "/" + status;
    $.post(varurl, {},
            {headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }}, "json").done(
            function (data) {
                var html = "";
                $.each(data.datahasil, function (index, arrdata) {
                    if(arrdata.nilai_angka_kredit == null){var ak = "-";}else{var ak = arrdata.nilai_angka_kredit;}
                    html += "<tr>";
                    html += "<td  align='center'>" + eval(index + 1) + "</td>";
                    html += "<td>" + arrdata.deskripsi_kegiatan + "</td>";
                    html += "<td>" + ak + "</td>";
                    html += "<td>" + arrdata.target_kuantitatif + " " + arrdata.satuan_target_kuantitatif + "</td>";
                    html += "<td>" + arrdata.target_kualitas + " % </td>";
                    html += "<td>" + arrdata.waktu + "  " + arrdata.satuan_waktu + " </td>";
                    html += "<td>" + arrdata.biaya + " Rupiah </td>";
                    if(arrdata.realisasi_kuantitatif == null && arrdata.realisasi_kualitas == null){
                        $(".rel").remove();
                    }else{                        
                        html += "<td>" + arrdata.realisasi_kuantitatif + " " + arrdata.satuan_target_kuantitatif + "</td>";
                        html += "<td>" + arrdata.realisasi_kualitas + " " + " % </td>";
                        html += "<td>" + arrdata.realisasi_waktu + " " + arrdata.satuan_waktu + " </td>";
                        html += "<td>" + arrdata.realisasi_biaya + " Rupiah</td>";
                    }
                    if(arrdata.realisasi_kuantitatif){
                        html += "<td id='action-button' align='center'><a onclick='hapuskpi(" + arrdata.id_kegiatan + ")' href='#'><i class='icon-trash'></i></a> - <a href='#' onclick='getdetailkpi(" + arrdata.id_kegiatan + ")'><i class='icon-pencil'></i></a></td>";
                    }else{
                        html += "<td></td><td></td><td></td><td></td><td id='action-button' align='center'><a onclick='hapuskpi(" + arrdata.id_kegiatan + ")' href='#'><i class='icon-trash'></i></a> - <a href='#' onclick='getdetailkpi(" + arrdata.id_kegiatan + ")'><i class='icon-pencil'></i></a></td>";
                    }
                    html += "</tr>";
                });
                if (status == 1 || status == '1')
                    $("#bodykpi").html(html);
                else if (status == 2 || status == '2')
                    $("#bodykpitambahan").html(html);
            });
}

function simpanrealisasi( ) {
    var varurl = getbasepath() + "skp/penilaian/addrealisasi";
    var datajab = {
        realisasi_kuantitatif: $("#e_realisasi_kuantitatif").val(),
        realisasi_kualitas: $("#e_realisasi_kualitas").val(),
        waktu: $("#e_waktu").val(),
        biaya: $("#e_biaya").val(),
        tahun: $("#tahun").val(),
        status: $("#e_status").val(),
        peg_id: $("#nikPegawai").val(),
    }
    var id_kegiatan = $("#e_id_kegiatan").val();
    if (id_kegiatan) {
        datajab['id_kegiatan'] = id_kegiatan;
    }

    var paramkirim = JSON.stringify(datajab);


    $.post(varurl, {"item": paramkirim},
    {headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }}, "json").done(
            function (data) {
                $('#modal_edit_kegiatan').modal('toggle');
                bootbox.alert(data.output);
                var html = "";
                $.each(data.datahasil, function (index, arrdata) {
                   html += "<tr>";
                    html += "<td  align='center'>" + eval(index + 1) + "</td>";
                    html += "<td>" + arrdata.deskripsi_kegiatan + "</td>";
                    html += "<td>" + arrdata.nilai_angka_kredit + "</td>";
                    html += "<td>" + arrdata.target_kuantitatif + " " + arrdata.satuan_target_kuantitatif + "</td>";
                    html += "<td>" + arrdata.target_kualitas + " % </td>";
                    html += "<td>" + arrdata.waktu + "  " + arrdata.satuan_waktu + " </td>";
                    html += "<td>" + arrdata.biaya + " Rupiah </td>";
                    html += "<td>" + arrdata.realisasi_kuantitatif + " " + arrdata.satuan_target_kuantitatif + "</td>";
                    html += "<td>" + arrdata.realisasi_kualitas + " " + " % </td>";
                    html += "<td>" + arrdata.realisasi_waktu + " " + arrdata.satuan_waktu + " </td>";
                    html += "<td>" + arrdata.realisasi_biaya + " Rupiah</td>";
                    html += "<td align='center'><a onclick='hapuskpi(" + arrdata.id_kegiatan + ")' href='#'><i class='icon-trash'></i></a> - <a href='#' onclick='getdetailkpi(" + arrdata.id_kegiatan + ")'><i class='icon-pencil'></i></a></td>";
                    html += "</tr>";
                });
                if (status == 1 || status == '1')
                    $("#bodykpi").html(html);
                else if (status == 2 || status == '2')
                    $("#bodykpitambahan").html(html);

                $('#modal_edit_kegiatan').modal('toggle');
            });
    window.location.reload();
}

function hapuskpi(databosku){
    var conf = confirm("Anda yakin ingin menghapus kegiatan ini ?");

    if(conf){
        var varurl = getbasepath() + "skp/pembuatan/deletekegiatan/" + databosku;

        $.post(varurl, {"idkeg": databosku}, 
        {headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }}, "json").done(window.location.reload());    
    }
}

function getdetailkpi(databosku){
    var varurl = getbasepath() + "skp/penilaian/getdetailkpi/" + databosku;

    $.post(varurl, {},
    {headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }}, "json").done(
            function (data) {
                $('#modal_edit_kegiatan').modal('toggle');
                // bootbox.alert(data.output);
                var datax = data.datahasil;
                $('#e_id_kegiatan').val(datax.id_kegiatan);
                $('#e_deskripsi_kegiatan').val(datax.deskripsi_kegiatan);
                $('#e_satuan_realisasi_kuantitatif').val(datax.satuan_target_kuantitatif);
                $('#e_satuan_kuantitatif').val(datax.satuan_target_kuantitatif);
                $('#e_satuan_realisasi_waktu').val(datax.satuan_waktu);
                $('#e_satuan_waktu').val(datax.satuan_waktu);  

                if(datax.realisasi_kuantitatif)
                    $('#e_realisasi_kuantitatif').val(datax.realisasi_kuantitatif);
                if(datax.realisasi_kualitas)
                    $('#e_realisasi_kualitas').val(datax.realisasi_kualitas);
                if(datax.realisasi_waktu)
                    $('#e_waktu').val(datax.realisasi_waktu);
                if(datax.realisasi_biaya)
                    $('#e_biaya').val(datax.realisasi_biaya); 

                if(datax.nilai_angka_kredit){
                    var ak = datax.nilai_angka_kredit;
                }else{
                    var ak = "-";
                }

                var html = '';
                html += "<tr>";
                html += "<td>" + datax.deskripsi_kegiatan + "</td>";
                html += "<td>" + ak + "</td>";
                html += "<td>" + datax.target_kuantitatif + " " + datax.satuan_target_kuantitatif +  "</td>";
                html += "<td>" + datax.target_kualitas + " %</td>";
                html += "<td>" + datax.waktu + " " + datax.satuan_waktu +  "</td>";
                html += "<td>" + datax.biaya + " Rupiah</td>";
                if(datax.status == 1)
                    $('#e_status').val("Pokok");
                else
                    $('#e_status').val("Tambahan");
                html += "</tr>";
                $('#bodytarget').html(html);
                // $('#modal-edit').html("<p> </p>");
            });
}